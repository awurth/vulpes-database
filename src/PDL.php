<?php

namespace Vulpes\Database;

class PDL
{
    public string $queryString;
    public array $queryParams;
    public float $execRuntime;
    public ?array $explainInfo;

    public function __construct(string $queryString, array $queryParams, float $startTime)
    {
        $this->queryString = $queryString;
        $this->queryParams = $queryParams;
        $this->execRuntime = round(microtime(true) - $startTime, 4);
        $this->explainInfo = null;
    }

    public function __toString(): string
    {
        return print_r($this, true);
    }

    public function __debugInfo(): array
    {
        return [
          'execRuntime' => $this->execRuntime,
          'parsedQuery' => $this->parseQuery(),
          'queryString' => $this->queryString,
          'queryParams' => $this->queryParams,
          'explainInfo' => $this->explainInfo
        ];
    }

    public function parseQuery(): string
    {
        $queryParams = [];
        foreach ($this->queryParams as $key => $val) {
            $queryParams[$this->prepareKey($key)] = $this->prepareValue($val);
        }
        uksort($queryParams, function ($a, $b) {
            return strlen($a) === strlen($b) ? 0 : (strlen($a) > strlen($b) ? -1 : 1);
        });
        return str_replace(array_keys($queryParams), array_values($queryParams), $this->queryString);
    }

    private function prepareKey(string $key)
    {
        return substr(trim($key), 0, 1) === ':' ? trim($key) : ':' . trim($key);
    }

    private function prepareValue($value): string
    {
        switch (gettype($value)) {
            case 'integer':
                return $value;
            case 'boolean':
                return $value ? 'TRUE' : 'FALSE';
            case 'NULL':
                return 'NULL';
        }
        return "\"{$value}\"";
    }
}