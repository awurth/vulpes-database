<?php

namespace Vulpes\Database;

/**
 * @property-read string $key
 * @property-read string $type
 * @property-read string $dsn
 * @property-read string $host
 * @property-read string $name
 * @property-read int    $port
 * @property-read string $char
 * @property-read string $user
 * @property-read string $pass
 * @property-read array  $options
 * @property-read string $version
 * @property-read string $role
 * @property-read string $dialect
 */
class Connection
{
    /**
     * MySQL
     */
    public const PDO_MYSQL = 'mysql';
    public const PDO_MYSQL_SOCKET = 'unix_socket';

    /**
     * SQLite 3 and SQLite 2
     */
    public const PDO_SQLITE = 'sqlite';

    /**
     * Cubrid
     */
    public const PDO_CUBRID = 'cubrid';

    /**
     * Microsoft SQL Server and Sybase Function
     */
    public const PDO_DBLIB = 'dblib';
    public const PDO_DBLIB_SYBASE = 'sybase';
    public const PDO_DBLIB_MSSQL = 'mssql';

    /**
     * Firebird
     */
    public const PDO_FIREBIRD = 'firebird';

    private array $storage;
    private bool $initialized = false;

    final public function __construct(string $key, array $data)
    {
        $this->storage = $data;
        $this->storage['key'] = $key;
        $this->storage['dsn'] = $this->createDSN();
        $this->storage['options'] = $this->getOptions();
        $this->initialized = true;
    }

    public function __debugInfo(): array
    {
        $info = $this->storage;
        $info['pass'] = '####';
        return $info;
    }

    public function __isset(string $name): bool
    {
        return array_key_exists($name, $this->storage);
    }

    public function __get(string $name)
    {
        if ($this->initialized === false) {
            if (isset($this->storage['type']) === false) {
                throw new RuntimeException("Missing connection \"type\" set in settings");
            }
            if (isset($this->storage[$name]) === false) {
                throw new RuntimeException("Missing property from \"{$this->type}\" connection settings: \"{$name}\"");
            }
        }
        return isset($this->storage[$name]) ? $this->storage[$name] : null;
    }

    public function __set(string $name, $value): void
    {
        if (is_null($this->dsn) || is_null($this->options) || is_null($this->key)) {
            $this->$name = $value;
        }
    }

    protected function createDSN(): string
    {
        switch ($this->type) {

            case self::PDO_MYSQL:
            case self::PDO_CUBRID:
                return "mysql:host={$this->host};dbname={$this->name};port={$this->port}";

            case self::PDO_SQLITE:
                return "sqlite:{$this->host}";

            case self::PDO_MYSQL_SOCKET:
                return "mysql:unix_socket={$this->host};dbname={$this->name}";

            case self::PDO_DBLIB:
            case self::PDO_DBLIB_MSSQL:
            case self::PDO_DBLIB_SYBASE:
                return $this->createDBLibDSN();

            case self::PDO_FIREBIRD:
                return $this->createFirebirdDSN();
        }

        throw new RuntimeException("Unknown PDO connection type '{$this->type}'.");
    }

    protected function getOptions(): array
    {
        $options = [
          PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
          PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        ];
        if (isset($this->char) === false) {
            return $options;
        }
        switch ($this->type) {
            case self::PDO_CUBRID:
            case self::PDO_MYSQL:
            case self::PDO_SQLITE:
                $options[PDO::MYSQL_ATTR_INIT_COMMAND] = 'SET NAMES ' . (isset($this->char) ? $this->char : 'utf8');
                break;
        }
        return $options;
    }

    protected function createDBLibDSN()
    {
        $port = (isset($this->port) && $this->port) ? ":{$this->port}" : '';
        $charset = (isset($this->char) && $this->char) ? "charset={$this->char};" : '';
        $version = (isset($this->version) && $this->version) ? "version={$this->version};" : '';

        return "{$this->type}:{$version}{$charset}host={$this->host}{$port};dbname={$this->name}";
    }

    private function createFirebirdDSN()
    {
        $role = (isset($this->role) && $this->role) ? "role={$this->role};" : '';
        $charset = (isset($this->char) && $this->char) ? "charset={$this->char};" : '';
        $dialect = (isset($this->dialect) && $this->dialect) ? "role={$this->dialect};" : '';

        return "firebird:{$charset}{$role}{$dialect}dbname={$this->name}";
    }
}