<?php

namespace Vulpes\Database;

use PDO as PHPDO;
use PDOException;
use PDOStatement;

class PDO extends PHPDO
{
    public const DEBUG_LEVEL_NONE = 0;
    public const DEBUG_LEVEL_LOG = 1;
    public const DEBUG_LEVEL_VERBOSE = 2;
    public const DEBUG_LEVEL_EXPLAIN = 4;
    public const DEBUG_LEVEL_EXIT = 8;

    public Connection $connection;

    /**
     * @var \Vulpes\Database\PDL[]
     */
    public array $debugLog = [];
    public int $debugLevel = self::DEBUG_LEVEL_NONE;
    public float $transactionSleepTime = 100000;
    public int $numberOfTransactionAttempts = 10;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
        parent::__construct($connection->dsn, $connection->user, $connection->pass, $connection->options);
    }

    public function execute(string $queryString, array $queryParams = []): PDOStatement
    {
        $timestamp = microtime(true);
        if ($this->debugLevel & self::DEBUG_LEVEL_EXPLAIN) {
            $queryString = 'EXPLAIN ' . $queryString;
        }
        $statement = $this->prepare($queryString);
        foreach ($queryParams as $parameter => $value) {
            $statement->bindValue($parameter, $value, $this->getParamType($value));
        }
        $statement->execute();
        if ($this->debugLevel) {
            $this->debug($statement, $queryParams, $timestamp);
        }
        return $statement;
    }

    public function call(string $procedure, ...$args): PDOStatement
    {
        $values = [];
        foreach ($args as $index => $value) {
            $values[':p_' . $index] = $value;
        }
        return $this->execute("call `{$procedure}`(" . implode(',', array_keys($values)) . ');', $values);
    }

    public function transaction(callable $callback, int $numberOfAttempt = 0): void
    {
        while (true) {
            try {
                $numberOfAttempt++;
                $this->beginTransaction();
                $callback();
                $this->commit();
                return;
            } catch (PDOException $exception) {
                $this->transactionError($exception, $numberOfAttempt);
            }
        }
    }

    private function transactionError(PDOException $exception, int $numberOfAttempt): void
    {
        if ($this->inTransaction()) {
            $this->rollBack();
        }
        if (
          $numberOfAttempt <= $this->numberOfTransactionAttempts &&
          strpos($exception, 'try restarting transaction') !== false
        ) {
            usleep($this->transactionSleepTime);
            return;
        }
        throw $exception;
    }

    private function getParamType($value): int
    {
        switch (gettype($value)) {
            case 'integer':
                return self::PARAM_INT;
            case 'boolean':
                return self::PARAM_BOOL;
            case 'NULL':
                return self::PARAM_NULL;
        }
        return self::PARAM_STR;
    }

    private function debug(PDOStatement $statement, array $queryParams, float $timestamp): void
    {
        $debugLog = new PDL($statement->queryString, $queryParams, $timestamp);

        if (self::DEBUG_LEVEL_EXPLAIN & $this->debugLevel) {
            $debugLog->explainInfo = $statement->fetchAll();
        }
        if (self::DEBUG_LEVEL_LOG & $this->debugLevel) {
            $this->debugLog[] = $debugLog;
        }
        if (self::DEBUG_LEVEL_VERBOSE & $this->debugLevel) {
            print $debugLog . PHP_EOL;
        }
        if (self::DEBUG_LEVEL_EXIT & $this->debugLevel) {
            exit;
        }
    }
}