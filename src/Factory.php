<?php

namespace Vulpes\Database;

class Factory
{
    /**
     * @var Connection[]
     */
    private array $connections = [];

    public function createPDO(string $key): PDO
    {
        if (is_null($connection = $this->getConnection($key))) {
            throw new RuntimeException('PDO Configuration settings not found by key: "' . $key . '"');
        }
        return new PDO($this->getConnection($key));
    }

    public function addConnection(Connection $connection): void
    {
        $this->connections[$connection->key] = $connection;
    }

    public function getConnection(string $key): ?Connection
    {
        return isset($this->connections[$key]) ? $this->connections[$key] : null;
    }
}