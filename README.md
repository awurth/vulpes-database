**A simple Database client.**

Example
```php
use Vulpes\Database\Connection;
use Vulpes\Database\Factory;
use Vulpes\Database\RuntimeException;

$connectionKey = Connection::PDO_SQLITE;

$database = new Factory;

try {

    $database->addConnection(new Connection($connectionKey, [
      'type' => Connection::PDO_SQLITE,
      'host' => ':memory:'
    ]));

    $pdo = $database->createPDO($connectionKey);

    $pdo->exec("CREATE TABLE `user`(`id` INTEGER PRIMARY KEY, `name` TEXT)");
    $pdo->exec("INSERT INTO `user`(`name`) VALUES ('Tom')");
    $pdo->exec("INSERT INTO `user`(`name`) VALUES ('Rebecca')");
    $pdo->exec("INSERT INTO `user`(`name`) VALUES ('Jim')");
    $pdo->exec("INSERT INTO `user`(`name`) VALUES ('Robert')");

    $pdo->debugLevel = $pdo::DEBUG_LEVEL_LOG;

    $user = $pdo->execute(
      'SELECT u.`id`, u.`name` FROM `user` AS u WHERE u.`id` = :userId',
      [
        ':userId' => 4
      ]
    )->fetchObject();

    print_r($user);

    $pdo->debugLevel = $pdo::DEBUG_LEVEL_LOG | $pdo::DEBUG_LEVEL_EXPLAIN;

    $user = $pdo->execute(
      'SELECT u.`id`, u.`name` FROM `user` AS u WHERE u.`id` = :userId',
      [
        ':userId' => 4
      ]
    )->fetchObject();

    print_r($pdo->debugLog);

} catch (RuntimeException $exception) {
    print $exception;
} catch (PDOException $exception) {
    print $exception;
}
```